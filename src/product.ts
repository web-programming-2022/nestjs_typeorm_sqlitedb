import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "ข้าวมันไม่ใช่ไก่"
    // product.price = 50;
    // await productRepository.save(product)
    // console.log("Saved a new user with id: " + product.id)

    console.log("Loading products from the database...")
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productRepository.findOneBy({id:1})
    console.log(updatedProduct)
    updatedProduct.price=180
    await productRepository.save(updatedProduct)

}).catch(error => console.log(error))
